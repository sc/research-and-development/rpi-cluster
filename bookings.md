# Booking sheet
## Cluster booking instructions  
Simply copy/paste the headlines of a previous project and change the details for your project.  
Add new entries above previous entries.  

### Cluster status
Not booked  

# Projects
## OpenBSD cluster
### Project description
The aim of this project is to explore if we can set up a useful OpenBSD cluster.
First we will reproduce the job scheduler (TR3S) project below and do couple of benchmarks.
Then we will repeat with OpenBSD.
Finally we'll compare the two approaches, both qualitatively (e.g. how easy to set it up) and quantitatively (compare the benchmarks).

### Participants
- Marco van Hulten
- Mustafa
- ...

### Project start and end
~~**Start:** 13/09/21  
**End:** 24/09/21~~ – postponed until further notice.

### Results


## Exploring compute job scheduling
### Project description
The aim of this project is to explore how a job scheduler works by manually building a very simple scheduler from scratch.  

### Participants
Oskar Vidarsson

### Project start and end
**Start:** 28/06/21  
**End:** 16/07/21

### Results
The code for the project is hosted here: https://github.com/oskarvid/tr3s  
**Features**  
* Round robin scheduling  
* Dynamic compute node discovery  
* Based on bash, no dependencies required  
