# Setting up the Raspberry Pi cluster
## Test setup
Today we got the last parts for the Raspberry Pi (RPi) cluster, the SD
cards.
They are preinstalled with *NOOBS*, containing Raspbian and LibreELEC,
containing a media centre (Kodi).
We used this for testing.

We decided to number the RPis in our stack from bottom to top (0, 1, 2,
3), where *0* is the master node.
We used a switch to connect computers to the designated Raspberry Pi
master to it (over Ethernet); the RPi host provides a DHCP service
by default.
On the clients we got an IP address, simply using `dhclient INTERFACE`.
On the RPi we enabled Wi-Fi with the UiB guest network.
We had to add a default gateway thus:

    route add default gw 10.113.0.1
    ping 9.9.9.9

This got a quick response, so now we have an internet connection as well
and can upgrade the Raspbian packages.

## Rough plan
Of course both the hardware and the software installed at this point is
only for testing.
We will want to:

1. Register the MAC address of the master RPi's ethernet interface
and/or the WAN interface of the router.
2. Install an operating system of our choice.
3. Couple the four different RPis together.
