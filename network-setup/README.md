* The `99_config.yaml` file goes in the `/etc/netplan` directory.  
* Apply changes with `netplan apply`  

Helpful documentation about netplan, dual NICs etc from Ubuntu: https://ubuntu.com/server/docs/network-configuration
