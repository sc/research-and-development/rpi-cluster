![rpi-cluster-graphic](https://git.app.uib.no/sc/rpi-cluster/-/raw/master/rpi-cluster-graphic.png)

This cluster is intended for experimentation and learning. The hope is that we will be able to try things on a cluster that we otherwise would not be able to on a full scale production system. It is your responsibility to set the cluster up. This documentation will get you started with the basics, the rest is up to you. If you've never done anything similar, this is your chance to learn by doing.  
Use the `bookings` file to book the cluster. Keep in mind that the project should be days to weeks long rather than several months long.  

## Previous projects done on the cluster
### The TR3S job scheduler  
The [Silly Simple Scheduler](https://github.com/oskarvid/tr3s) (aka TR3S) was an exploration of how a job scheduler works. It was put together with not-so-beautiful code in bash and its features included [first in first out](https://en.wikipedia.org/wiki/FIFO_(computing_and_electronics)) (FIFO) job scheduling and dynamic addition of new compute nodes. It is in no way intended to work well, use and abuse at your own risk!  

## Using NOOBS to install Raspberry Pi OS (Raspbian)
The [official documentation](https://www.raspberrypi.org/software/) has you covered with details, in short you can run `sudo apt install rpi-imager` and install one of the default OS'es that NOOBS offers. Since we have four 8GB RAM Raspberry Pi's you should select a 64 bit OS version.  
There are lots of other operating systems available, e.g [freeBSD](https://wiki.freebsd.org/action/show/arm/Raspberry%20Pi?action=show&redirect=FreeBSD%2Farm%2FRaspberry+Pi), [nixOS](https://nixos.wiki/wiki/NixOS_on_ARM/Raspberry_Pi_4), [Arch Linux](https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-4), and [OpenSUSE](https://en.opensuse.org/HCL:Raspberry_Pi4).

## Configuring the Raspberry Pi
There are a ton of supported configuration options, the ones suggested are listed below, but in case you need something more, [the official documentation](https://www.raspberrypi.org/documentation/configuration/) is a good place to start.

### Enable SSH on first boot
Create a file named "ssh" on the boot partition as described in point 3 in [this tutorial](https://www.raspberrypi.org/documentation/remote-access/ssh/). Also consider manually adding your public ssh key in the `/home/pi/.ssh/authorized_keys` file.  

### Disable automatic login to desktop
Run `sudo raspi-config` from the command line and disable automatic login to desktop.

## Connecting the hardware together
You need the following hardware components to set the cluster up.  
* 4x Raspberry Pi's
* 4x SD cards with your OS of choice
* 6x Network cables
    * 4x from the Raspberry Pi's to the switch
    * 1x from the switch to the wall
    * 1x from your computer to the switch
* 4x USB-C power cables
* 1x USB power supply
* 1x Network switch
* 1x HDMI to mini-HDMI
    * Ideally only used for troubleshooting and initial setup
    
The Raspberry Pi MAC addresses need to be whitelisted for your network outlet, open a ticket with [the help desk](hjelp.uib.no) and ask them to whitelist these MAC addresses:  
```
rpi 1: e4:5f:01:0b:1b:4c
rpi 2: e4:5f:01:0b:1a:e0
rpi 3: e4:5f:01:0b:1a:ad
rpi 4: e4:5f:01:0b:1a:d4
```
Your network outlet ID should be written on it.

## FAQ
* How do I find the IP addresses of my nodes?
    * The bruteforce method is to use the HDMI -> mini HDMI connector and run `ifconfig` in a terminal
* What can I do with it?
    * Be creative, that's what you can do! :)
